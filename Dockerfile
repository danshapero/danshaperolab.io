FROM firedrakeproject/firedrake@sha256:067547361ce9abd800385441a7df140ef27c5c14098cbb56da55e0d4ee87b2e5

# Get gmsh and other compiled dependencies
RUN sudo apt-get update && \
    sudo apt-get install -yq \
    ffmpeg \
    graphviz \
    libgraphviz-dev \
    libglu1-mesa \
    libxcursor1 \
    libxinerama1

RUN curl -O http://gmsh.info/bin/Linux/gmsh-4.4.1-Linux64.tgz && \
    tar xvf gmsh-4.4.1-Linux64.tgz && \
    sudo cp gmsh-4.4.1-Linux64/bin/gmsh /usr/bin/

# Hack to activate the virtual environment
ENV PATH=/home/firedrake/firedrake/bin:$PATH \
    VIRTUAL_ENV=/home/firedrake/firedrake \
    OMP_NUM_THREADS=1

# Install jupyter and make a kernel for this virtual environment
RUN pip3 install jupyter ipykernel
RUN python3 -m ipykernel install --user --name=firedrake

# Get nikola, the static site generator, and other Python dependencies
RUN pip3 install nikola tqdm pygraphviz
