{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ladyzhenskaya-Babuska-Brezzi\n",
    "\n",
    "In previous post, I showed how solving the discretized form of the Stokes equations is more involved because (1) the system is indefinite and (2) it has a non-trivial null space that we need to project out.\n",
    "What I haven't shown is what discretizations of the Stokes equations result in linear systems that are solvable at all.\n",
    "\n",
    "In the following, I'll introduce a more high-level way of viewing the Stokes equations.\n",
    "This functional analytic perspective is helpful in abstracting away some of the details of the problem (divergences, gradients, etc.) and getting at the heart of constrained optimization in Banach spaces.\n",
    "To that end, let $V$ be the Banach space where $u$ lives, and let $A$ be a bounded self-adjoint operator from $V$ to its dual space $V^*$.\n",
    "We want to find the critical point of the functional\n",
    "\n",
    "$$\\mathscr{J}(u) = \\frac{1}{2}\\langle Au, u\\rangle - \\langle f, u\\rangle$$\n",
    "\n",
    "subject to the constraint that\n",
    "\n",
    "$$Bu = 0,$$\n",
    "\n",
    "where $B$ is a map from $V$ into the dual of another Banach space $Q$.\n",
    "In our case, $V$ is a space of differentiable vector fields and $Q$ is a space of scalar fields.\n",
    "To enforce this constraint, we will introduce a Lagrange multiplier $p$ that lives in $Q$ and find a critical point of the Lagrangian\n",
    "\n",
    "$$\\mathscr{L}(u, p) = \\mathscr{J}(u) + \\langle Bu, p\\rangle.$$\n",
    "\n",
    "The weak form of the optimality system for $u$ and $p$ is that\n",
    "\n",
    "$$\\begin{align}\n",
    "\\langle Au + B^*p, v\\rangle & = \\langle f, v\\rangle \\\\\n",
    "\\langle Bu, q\\rangle & = 0\n",
    "\\end{align}$$\n",
    "\n",
    "for all $v$ in $V$ and $q$ in $Q$.\n",
    "\n",
    "Assuming that a solution pair $u$, $p$ exists, we want to establish bounds on the norms of $u$ and $p$ in terms of the norm of $f$.\n",
    "First, we need to assume that $A$ is bounded and *coercive*:\n",
    "\n",
    "$$\\lambda\\|v\\|^2 \\le \\langle Av, v\\rangle \\le \\Lambda\\|v\\|^2$$\n",
    "\n",
    "for some constants $\\lambda$, $\\Lambda$ and for all $v$.\n",
    "Next, note that\n",
    "\n",
    "$$\\langle B^*p, u\\rangle = \\langle Bu, p\\rangle = 0,$$\n",
    "\n",
    "first by definition of the adjoint operator and then by the second equation of the optimality system.\n",
    "We can then proceed to write\n",
    "\n",
    "$$\\lambda\\|u\\|^2 \\le \\langle Au, u\\rangle = \\langle Au + B^*p, u\\rangle = \\langle f, u\\rangle \\le \\|f\\|\\cdot\\|u\\|.$$\n",
    "\n",
    "If we divide by $\\lambda$ we get that\n",
    "\n",
    "$$\\|u\\| \\le \\lambda^{-1}\\|f\\|.$$\n",
    "\n",
    "This should all be fairly familiar if you've studied the convergence theory of Galerkin methods for the Poisson equation.\n",
    "\n",
    "Next we need to establish some conditions on the constraint operator $B$.\n",
    "You'll often see these described as the *inf-sup* conditions, but I'll write them in a slightly different form first.\n",
    "Suppose that, for all $q$,\n",
    "\n",
    "$$\\|B^*q\\| \\ge \\beta\\|q\\|.$$\n",
    "\n",
    "This implies that $B^*$ is injective, which is exactly what we need to bound the norm of $p$.\n",
    "Note that the range of $B^*$ is the dual space $V^*$ of $V$, so the norm in the last equation is defined as\n",
    "\n",
    "$$\\|Bq\\| \\equiv \\sup_{\\|v\\| = 1}\\langle B^*q, v\\rangle.$$\n",
    "\n",
    "Using the second equation of the optimality system, we then find that\n",
    "\n",
    "$$\\beta\\|p\\| = \\sup_{\\|v\\| = 1}\\langle B^*p, v\\rangle = \\sup_{\\|v\\| = 1}\\langle f, v\\rangle - \\langle Au, v\\rangle\\ldots$$\n",
    "\n",
    "Now, since $A$ is self-adjoint and positive-definite, the Cauchy-Schwarz inequality implies that\n",
    "\n",
    "$$|\\langle Au, v\\rangle| \\le \\sqrt{\\langle Au, u\\rangle}\\cdot\\sqrt{\\langle Av, v\\rangle}.$$\n",
    "\n",
    "We can then bound the last equation as follows:\n",
    "\n",
    "$$\\ldots \\le \\|f\\| + \\Lambda\\|u\\| \\le \\left(1 + \\frac{\\Lambda}{\\lambda}\\right)\\|f\\|$$\n",
    "\n",
    "where in the first inequalty we used the fact that $\\|v\\| = 1$ and in the second we reused the estimate that $\\|u\\| \\le \\lambda^{-1}\\|f\\|$.\n",
    "Putting all of this together, we find that\n",
    "\n",
    "$$\\|p\\| \\le \\beta^{-1}\\left(1 + \\frac{\\Lambda}{\\lambda}\\right)\\|f\\|.$$\n",
    "\n",
    "So, the LBB conditions imply a bound for the norm of the pressure field."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Back to fluids\n",
    "\n",
    "This was all very abstract, so let's re-examine everything in terms of the Stokes equations again.\n",
    "The operator $A$ acts on functions as\n",
    "\n",
    "$$\\langle Au, v\\rangle \\equiv \\int_\\Omega 2\\mu\\dot\\varepsilon(u):\\dot\\varepsilon(v)dx.$$\n",
    "\n",
    "If we have homogeneous boundary conditions, we can define $V$ as the space of all vector fields that vanish on the boundary of the domain.\n",
    "In that case the Poincare inequality implies the boundedness and coercivity of $A$.\n",
    "Next is the constraint operator $B$, which acts on functions as\n",
    "\n",
    "$$\\langle Bv, q\\rangle \\equiv \\int_\\Omega (\\nabla\\cdot v)\\, q\\, dx.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Discretization\n",
    "\n",
    "Unfortunately, the fact that the full infinite-dimensional problem satisfies the LBB conditions does not imply that a discretization does too."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "nikola": {
   "category": "",
   "date": "2019-12-09 16:23:48 UTC-08:00",
   "description": "",
   "link": "",
   "slug": "stokes2",
   "tags": "",
   "title": "Stokes II",
   "type": "text"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
